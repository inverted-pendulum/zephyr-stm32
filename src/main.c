#include <device.h>
#include <drivers/gpio.h>
#include <drivers/pwm.h>
#include <inttypes.h>
#include <math.h>
#include <stdlib.h>
#include <sys/printk.h>
#include <sys/util.h>
#include <zephyr.h>

/* ========================================================================== */

#define MY_LED_NODE DT_ALIAS(my_led0)
#if !DT_NODE_HAS_STATUS(MY_LED_NODE, okay)
#error "my-led1 devicetree alias is not defined"
#endif

#define MOTOR_NODE DT_ALIAS(my_led1)
#if !DT_NODE_HAS_STATUS(MOTOR_NODE, okay)
#error "my-led1 devicetree alias is not defined"
#endif

#define MY_LED_NODE2 DT_ALIAS(my_led2)
#if !DT_NODE_HAS_STATUS(MY_LED_NODE2, okay)
#error "my-led1 devicetree alias is not defined"
#endif

#define IN0_NODE DT_ALIAS(input0)
#if !DT_NODE_HAS_STATUS(IN0_NODE, okay)
#error "input0 devicetree alias is not defined"
#endif

#define IN1_NODE DT_ALIAS(input1)
#if !DT_NODE_HAS_STATUS(IN1_NODE, okay)
#error "input1 devicetree alias is not defined"
#endif

#define PWM_LED0_NODE DT_ALIAS(pwm_led0)
#if !DT_NODE_HAS_STATUS(PWM_LED0_NODE, okay)
#error "pwm-led0 devicetree alias is not defined"
#endif

#define PWM_CTLR DT_PWMS_CTLR(PWM_LED0_NODE)
#define PWM_CHANNEL DT_PWMS_CHANNEL(PWM_LED0_NODE)
#define PWM_FLAGS DT_PWMS_FLAGS(PWM_LED0_NODE)
#define PWM_NODE DEVICE_DT_GET(PWM_CTLR)

#define PWM_LED1_NODE DT_ALIAS(pwm_led1)
#if !DT_NODE_HAS_STATUS(PWM_LED1_NODE, okay)
#error "pwm-led1 devicetree alias is not defined"
#endif

#define PWM1_CTLR DT_PWMS_CTLR(PWM_LED1_NODE)
#define PWM1_CHANNEL DT_PWMS_CHANNEL(PWM_LED1_NODE)
#define PWM1_FLAGS DT_PWMS_FLAGS(PWM_LED1_NODE)
#define PWM1_NODE DEVICE_DT_GET(PWM1_CTLR)

#define PWM_LED2_NODE DT_ALIAS(pwm_led2)
#if !DT_NODE_HAS_STATUS(PWM_LED2_NODE, okay)
#error "pwm-led2 devicetree alias is not defined"
#endif

#define PWM2_CTLR DT_PWMS_CTLR(PWM_LED2_NODE)
#define PWM2_CHANNEL DT_PWMS_CHANNEL(PWM_LED2_NODE)
#define PWM2_FLAGS DT_PWMS_FLAGS(PWM_LED2_NODE)
#define PWM2_NODE DEVICE_DT_GET(PWM2_CTLR)

/* ========================================================================== */

struct gpio_dt_spec my_led = GPIO_DT_SPEC_GET(MY_LED_NODE, gpios);
struct gpio_dt_spec my_led2 = GPIO_DT_SPEC_GET(MY_LED_NODE2, gpios);
struct gpio_dt_spec motor = GPIO_DT_SPEC_GET(MOTOR_NODE, gpios);

struct gpio_dt_spec in1 = GPIO_DT_SPEC_GET(IN0_NODE, gpios);
struct gpio_dt_spec in2 = GPIO_DT_SPEC_GET(IN1_NODE, gpios);

static struct gpio_callback callback_data1;
static struct gpio_callback callback_data2;

const struct device* pwm = PWM_NODE;
const struct device* pwm1 = PWM1_NODE;
const struct device* pwm2 = PWM2_NODE;

/* ========================================================================== */

#define SLEEP_TIME_MS 1000
#define RESOLUTION 2400
#define MAX_FREQ 50
#define MIN_PERIOD (1000.0 / MAX_FREQ)
#define MAX_TORQUE 2

volatile int32_t angle = RESOLUTION / 2;
volatile bool angle_update = true;
const double scale = (double)RESOLUTION / 360;

/* ========================================================================== */

void angle_add(const int i) {
    angle += i;

    while (angle < 0) {
        angle += RESOLUTION;
    }

    while (angle > RESOLUTION) {
        angle -= RESOLUTION;
    }

    angle_update = true;
}

void callback1(const struct device* dev, struct gpio_callback* cb,
               uint32_t pins) {
    int v1 = gpio_pin_get_dt(&in1);
    int v2 = gpio_pin_get_dt(&in2);

    if (v1 == v2) {
        angle_add(-1);
    } else {
        angle_add(1);
    }
}

void callback2(const struct device* dev, struct gpio_callback* cb,
               uint32_t pins) {
    int v1 = gpio_pin_get_dt(&in1);
    int v2 = gpio_pin_get_dt(&in2);

    if (v1 == v2) {
        angle_add(-1);
    } else {
        angle_add(-1);
    }
}

/* ========================================================================== */

void my_timer_handler(struct k_timer* dummy) {
    printk("Angle x10: %d\n", (int)(10 * (double)angle / scale));
}

K_TIMER_DEFINE(my_timer, my_timer_handler, NULL);

/* ========================================================================== */

typedef void (*interupt_callback)(const struct device*, struct gpio_callback*,
                                  uint32_t);

bool init_gpio_with_interupt(const struct gpio_dt_spec* gpio,
                             struct gpio_callback* callback_data,
                             interupt_callback callback) {
    int error;

    if (!device_is_ready(gpio->port)) {
        printk("Error: gpio %s is not ready\n", gpio->port->name);
        return false;
    }

    if (!device_is_ready(gpio->port)) {
        error = gpio_pin_configure_dt(gpio, GPIO_INPUT);
        if (error != 0) {
            printk("Error %d: failed to configure %s pin %d\n", error,
                   gpio->port->name, gpio->pin);
            return false;
        }
    }

    error = gpio_pin_configure(gpio->port, gpio->pin,
                               GPIO_NOPULL | GPIO_ACTIVE_HIGH | GPIO_INPUT);
    if (error != 0) {
        printk("Error %d: failed to configure %s pin %d\n", error,
               gpio->port->name, gpio->pin);
        return false;
    }

    error = gpio_pin_interrupt_configure_dt(gpio, GPIO_INT_EDGE_BOTH);
    if (error != 0) {
        printk("Error %d: failed to configure interrupt on %s pin %d\n", error,
               gpio->port->name, gpio->pin);
        return false;
    }

    gpio_init_callback(callback_data, callback, BIT(gpio->pin));
    gpio_add_callback(gpio->port, callback_data);

    printk("Set up interrupt at %s pin %d\n", gpio->port->name, gpio->pin);

    return true;
}

/* ========================================================================== */

bool init_gpio_output(struct gpio_dt_spec* gpio) {
    int error;
    if (gpio->port && !device_is_ready(gpio->port)) {
        printk("Error: gpio device %s is not ready; ignoring it\n",
               gpio->port->name);
        gpio->port = NULL;
        return false;
    }

    if (gpio->port) {
        error = gpio_pin_configure_dt(gpio, GPIO_OUTPUT);
        if (error != 0) {
            printk("Error %d: failed to configure gpio device %s pin %d\n",
                   error, gpio->port->name, gpio->pin);
            gpio->port = NULL;
            return false;
        } else {
            printk("Set up output at %s pin %d\n", gpio->port->name, gpio->pin);
        }
    }

    if (!gpio->port) {
        return false;
    }

    return true;
}

/* ========================================================================== */

double abs_d(double d) {
    return (d > 0) ? d : -d;
}

double calculate_torque() {
    double torque = 0.05 + abs_d(180.0 - (double)angle / scale) / 10;

    if (torque > MAX_TORQUE) {
        torque = MAX_TORQUE;
    }

    return torque;
}

double calculate_period() {
    return (double)MIN_PERIOD * (double)MAX_TORQUE / calculate_torque();
}

/* ========================================================================== */

bool init_pwm(const struct device* pwm) {
    if (!device_is_ready(pwm)) {
        printk("Error: PWM device %s is not ready\n", pwm->name);
        return false;
    }

    return true;
}

/* ========================================================================== */

void main(void) {
    if (!init_gpio_with_interupt(&in1, &callback_data1, callback1) ||
        !init_gpio_with_interupt(&in2, &callback_data2, callback2) ||
        !init_pwm(pwm) || !init_pwm(pwm1) || !init_pwm(pwm2)) {
        return;
    }

    printk("Hello\n");
    k_timer_start(&my_timer, K_NO_WAIT, K_MSEC(1000));

    uint32_t period = 1000 * 4000;
    if (pwm_pin_set_usec(pwm, PWM_CHANNEL, period, period * 3 / 4U,
                         PWM_FLAGS)) {
        printk("Fail!\n");
    }

    if (pwm_pin_set_usec(pwm1, PWM1_CHANNEL, period, period * 2 / 4U,
                         PWM1_FLAGS)) {
        printk("Fail!\n");
    }

    if (pwm_pin_set_usec(pwm2, PWM2_CHANNEL, period, period * 1 / 4U,
                         PWM2_FLAGS)) {
        printk("Fail!\n");
    }

    while (true) {

        if (angle_update) {
            angle_update = false;
        }
    }
}
